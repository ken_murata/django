from django.shortcuts import render, redirect, get_object_or_404,HttpResponse
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, LogoutView
from django.views import generic
from django.urls import reverse_lazy
from django.contrib import messages
from django.db.models import Q

from .models import Document, Comment, CustomUser
from .forms import DocumentForm, CommentForm, LoginForm, SignupForm

class IndexView(generic.ListView):
    template_name = 'movie/index.html'
    context_object_name = 'latest_file_list'

    def get_queryset(self):
        q_word = self.request.GET.get('query')
 
        if q_word:
            object_list = Document.objects.filter(
                Q(description__icontains=q_word) | Q(username__icontains=q_word)).order_by('-uploaded_at')
        else:
            object_list = Document.objects.order_by('-uploaded_at')
        return object_list

class UserView(generic.ListView):
    template_name = 'movie/user.html'
    context_object_name = 'latest_file_list'

    def get_queryset(self):
        username = self.request.path_info.split('/')[-1]
        user_id = CustomUser.objects.filter(username=username).values_list('id')[0][0]
        return Document.objects.filter(user_id=user_id).order_by('-uploaded_at')[:3]

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        username = self.request.path_info.split('/')[-1]
        like_documents =  CustomUser.objects.filter(username=username).values('like_document')
        
        context['username'] = username
        context['like_documents'] = Document.objects.filter(id__in=like_documents).order_by('-uploaded_at')
        return context
        
class Login(LoginView):
    from_class = LoginForm
    template_name = 'movie/login.html'

class Logout(LogoutView):
    template_name = 'movie/index.html'

class DetailView(generic.DetailView):
    model = Document
    template_name = 'movie/detail.html'
    def get_context_data(self,**kwargs):
        document_id = self.request.path_info.split('/')[-2]
        user = self.request.user
        context = super().get_context_data(**kwargs)
        context['delete'] = str(user)==str(Document.objects.filter(pk=document_id).values_list('username')[0][0])
        follower_id = Document.objects.filter(pk=document_id).values('follower')
        context['follower'] = CustomUser.objects.filter(id__in=follower_id)
        return context

class DeleteView(generic.DeleteView):
    model = Document
    form = DocumentForm

    success_url = reverse_lazy('movie:index')

    def delete(self, request, *args, **kwargs):
        result = super().delete(request, *args, **kwargs)
        messages.success(
            self.request, '「{}」を削除しました'.format(self.object))
        return result

@login_required
def add_comment(request, pk):
    post = get_object_or_404(Document, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.author = request.user
            comment.save()
            return redirect('movie:detail',pk=pk)
    else:
        form = CommentForm()
    return render(request, 'movie/add_comment.html', {'form': form})
    
def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(request,username=username,password=raw_password)
            if user is not None:
                login(request,user)
            return redirect('movie:index')
    else:
        form = SignupForm()
    return render(request, 'movie/signup.html', {'form':form})

@login_required
def model_form_upload(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        form.instance.username = request.user
        form.instance.user = request.user
        if form.is_valid():
            form.save()
            return redirect('movie:index')
    else:
        form = DocumentForm()
    return render(request, 'movie/upload.html', {
        'form': form
    })

@login_required
def edit_profile(request):
    return HttpResponse("You're editing")

@login_required
def comment_approve(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.approve()
    return redirect('movie:index')

@login_required
def comment_remove(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.delete()
    return redirect('movie:index')

@login_required
def like_document(request, pk):
    document = get_object_or_404(Document, pk=pk)
    if document:
        document.follower.add(request.user)
        request.user.like_document.add(document)
    return redirect('movie:detail',pk=pk)