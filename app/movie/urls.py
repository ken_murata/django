from django.urls import path
from django.conf.urls.static import static

from . import views

app_name = 'movie'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('upload', views.model_form_upload, name='upload'),
    path('edit_profile', views.edit_profile, name='edit_profile'),
    path('<int:pk>/',views.DetailView.as_view(),name='detail'),
    path('<int:pk>/remove',views.DeleteView.as_view(),name='remove'),
    path('<int:pk>/comment/',views.add_comment,name='add_comment'),
    path('<int:pk>/comment/approve/',views.comment_approve,name='comment_approve'),
    path('<int:pk>/comment/remove/',views.comment_remove,name='comment_remove'),
    path('login/',views.Login.as_view(),name='login'),
    path('logout/',views.Logout.as_view(),name='logout'),
    path('signup/',views.signup,name='signup'),
    path('user/<str:username>',views.UserView.as_view(),name='user'),
    path('<int:pk>/like',views.like_document,name='like_document'),
]